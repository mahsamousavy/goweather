package goweathertest;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import goweathertest.Constants;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class GoWeatherTest {
	
	static AppiumDriver<MobileElement> driver;
	
	public static void main(String[] args) {
		
		try {
			executeGoWeather();
		}catch(Exception exp) {
			System.out.println(exp.getCause());
			System.out.println(exp.getMessage());
			exp.printStackTrace();
		}
		
	}
	private static void setCapabilities(DesiredCapabilities capabilities, Constants constants) {
		capabilities.setCapability("platformName", constants.platformName);
        capabilities.setCapability("deviceName", constants.deviceName);
        capabilities.setCapability("platformVersion", constants.platformName);
        //NOTE: 📣 Download this application from https://myket.ir/app/com.gau.go.launcherex.gowidget.weatherwidget
        capabilities.setCapability("app", constants.appAddress);
        capabilities.setCapability("udid", constants.udid);
        capabilities.setCapability("appActivity", constants.appActivity);
        capabilities.setCapability("appPackage", constants.appPackage);
        //NOTE: 📣 Run in emulator
//        capabilities.setCapability("avd", constants.emulator);
	}
	private static void openApplication(DesiredCapabilities capabilities, Constants constants) throws MalformedURLException {
		 driver = new AppiumDriver<MobileElement>(new URL(constants.driverAddress), capabilities);
         System.out.println("Application Started....");
         driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	private static void citySearch(Constants constants) {
		MobileElement citySearchInput = driver.findElement(By.id(constants.citySearchInput));
        citySearchInput.click();
        citySearchInput.sendKeys(constants.tehranCity);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	private static void citySelect(Constants constants) {
		MobileElement cityTehran = driver.findElementByXPath(constants.tehranCityText);
        cityTehran.click();
        try {
	         driver.findElement(By.id(constants.goIt1Id)).click();
	         driver.findElement(By.id(constants.goIt2Id)).click();
	         driver.findElement(By.id(constants.goIt3Id)).click();
        }catch(Exception exp) {
        }
	}
	private static void selectForecast(Constants constants) {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
   	 driver.findElement(By.xpath(constants.forecastText)).click();
	}
	private static void scrollDown(Constants constants) {
		driver.findElement(MobileBy.AndroidUIAutomator(constants.scrollToText));
	}
	private static void takeScreenShot(Constants constants) {
		File file  = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(file, new File(constants.screenShotName));
		}catch(Exception exp) {
        }
        System.out.println("Application Stoped....");
     	driver.quit();
	}
	
	public static void executeGoWeather() throws Exception {
		Constants constants= new Constants();
		DesiredCapabilities capabilities = new DesiredCapabilities();
		setCapabilities(capabilities, constants);
		openApplication(capabilities, constants);
		citySearch(constants);
		citySelect(constants);
		selectForecast(constants);
		scrollDown(constants);
		takeScreenShot(constants);
    	
	}		
}