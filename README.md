# GoWeather
The purpose of this project is to automate GoWeather's application testing in Android mobile process according to testcase.

# Getting Started
Clone the project:
git clone https://gitlab.com/mahsamousavy/goweather

# Installation
Download (https://github.com/appium/appium-desktop/releases/) and setup Appium (see http://appium.io/docs/en/about-appium/getting-started/)
To be able to run these tests you need to have eclipse installed on your computer. 
https://www.eclipse.org/downloads/

# Install libraries
You need also add library selenium java and Appium java client from https://mvnrepository.com/ such as:

``` code
<dependencies>
  <!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java -->
<dependency>
    <groupId>org.seleniumhq.selenium</groupId>
    <artifactId>selenium-java</artifactId>
    <version>3.141.59</version>
</dependency>
  
  <!-- https://mvnrepository.com/artifact/io.appium/java-client -->
<dependency>
    <groupId>io.appium</groupId>
    <artifactId>java-client</artifactId>
    <version>7.5.1</version>
</dependency>
</dependencies>
```
You need udid mobile device with run command adb devices
Then you need start Appium client

# setup in appium
``` code
{
  "udid": "8A5Y0FEF5",
  "appPackage": "com.gau.go.launcherex.gowidget.weatherwidget",
  "appActivity": "com.jiubang.goweather.GOWeatherActivity",
  "platformName": "android",
  "deviceName": "Pixel 3 XL",
  "unicodeKeyboard": "true",
  "resetKeyboard": "true",
Note: you need download apk file GoWeather application
  "app": "/Users/Mahsaa/Documents/android/GOWeatherEX_base.apk"

}
```
For run in emulator must add emulator device in android studio and add 
"avd": "Pixel” in project
I used mobile device pixle 3xl with android version 11.


General project rules:

Naming rules:

No upper case letters
No spaces
No symbols
No dashes.
